package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BuscadorPersonas extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextField nombreTB;
	private JTextField apTB;
	private JTextField amTB;
	private JButton guardarBtn;
	private JTextField buscarTb;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscadorPersonas frame = new BuscadorPersonas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscadorPersonas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 569, 447);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(92, 190, 326, 183);
		contentPane.add(scrollPane);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(10, 41, 123, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidoPaterno = new JLabel("Apellido Paterno:");
		lblApellidoPaterno.setBounds(10, 66, 170, 14);
		contentPane.add(lblApellidoPaterno);
		
		JLabel lblApellidoMaterno = new JLabel("Apellido Materno:");
		lblApellidoMaterno.setBounds(10, 96, 170, 14);
		contentPane.add(lblApellidoMaterno);
		
		nombreTB = new JTextField();
		nombreTB.setBounds(208, 38, 152, 20);
		contentPane.add(nombreTB);
		nombreTB.setColumns(10);
		
		apTB = new JTextField();
		apTB.setBounds(208, 63, 152, 20);
		contentPane.add(apTB);
		apTB.setColumns(10);
		
		amTB = new JTextField();
		amTB.setBounds(208, 93, 152, 20);
		contentPane.add(amTB);
		amTB.setColumns(10);
		
		guardarBtn = new JButton("Nuevo");
		guardarBtn.setBounds(393, 37, 137, 23);
		contentPane.add(guardarBtn);
		
		JButton btnNewButton_1 = new JButton("Modificar");
		btnNewButton_1.setBounds(393, 62, 137, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Eliminar");
		btnNewButton_2.setBounds(393, 92, 137, 23);
		contentPane.add(btnNewButton_2);
		
		JLabel lblBuscar = new JLabel("Buscar:");
		lblBuscar.setBounds(92, 165, 46, 14);
		contentPane.add(lblBuscar);
		
		buscarTb = new JTextField();
		buscarTb.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});
		buscarTb.setBounds(195, 162, 223, 20);
		contentPane.add(buscarTb);
		buscarTb.setColumns(10);
	}

	public JButton getGuardarBtn() {
		return guardarBtn;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JTextField getNombreTB() {
		return nombreTB;
	}

	public JTextField getApTB() {
		return apTB;
	}

	public JTextField getAmTB() {
		return amTB;
	}

	public JTextField getBuscarTb() {
		return buscarTb;
	}
	
	
}
