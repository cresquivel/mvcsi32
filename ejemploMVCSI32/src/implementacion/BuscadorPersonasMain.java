package implementacion;

import controller.BuscadorPersonaController;
import model.tableModels.PersonaTableModel;
import view.BuscadorPersonas;

public class BuscadorPersonasMain {
	public static void main(String args[]) {
		PersonaTableModel model = new PersonaTableModel();
		BuscadorPersonas view = new BuscadorPersonas();
		BuscadorPersonaController controller = 
				new BuscadorPersonaController(model,view);
		view.setVisible(true);
	}
}