package controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTable;

import model.dao.PersonaDAO;
import model.entities.Persona;
import model.tableModels.PersonaTableModel;
import view.BuscadorPersonas;

public class BuscadorPersonaController {
	PersonaTableModel tableModel;
	BuscadorPersonas vista;
	PersonaDAO dao = new PersonaDAO();
	JTable tabla;

	public BuscadorPersonaController(PersonaTableModel tableModel, BuscadorPersonas vista) {
		super();
		this.tableModel = tableModel;
		this.vista = vista;
		tabla = new JTable(tableModel);
		vista.getScrollPane().setViewportView(tabla);
		vista.getGuardarBtn().addActionListener(e -> guardarPersona());
		vista.getBuscarTb().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println(BuscadorPersonaController
						.this.vista.getBuscarTb().getText());
				BuscadorPersonaController.
				this.buscarPersona(BuscadorPersonaController
						.this.vista.getBuscarTb().getText());
			}
		});
	}

	private void buscarPersona(String palabraDeBusqueda) {
		this.tableModel.buscar(palabraDeBusqueda);
		tabla.setModel(tableModel);
		vista.getScrollPane().setViewportView(tabla);
	}

	private void guardarPersona() {
		Persona p = new Persona();
		p.setNombre(vista.getNombreTB().getText());
		p.setApellidoPaterno(vista.getApTB().getText());
		p.setApellidoMaterno(vista.getAmTB().getText());
		dao.agregar(p);
		this.tableModel.cargarTodos();
		tabla.setModel(tableModel);
		vista.getScrollPane().setViewportView(tabla);
	}
}
