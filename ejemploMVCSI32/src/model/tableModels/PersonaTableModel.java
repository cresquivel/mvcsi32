package model.tableModels;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.dao.PersonaDAO;
import model.entities.Persona;

public class PersonaTableModel extends AbstractTableModel{
	
	PersonaDAO dao = new PersonaDAO();
	List<Persona> personas = new ArrayList<Persona>();
	List<Persona> personasAux = new ArrayList<Persona>();
	
	public PersonaTableModel() {
		cargarTodos();
	}
	
	public void cargarTodos() {
		// Cargo todos los objetos de tipo personas
		// desde la base de datos y los almaceno en el ArrayList persona
		personas = dao.buscarTodos();
		// instancio la lista personasAux con todos los elementos de persona
		personasAux = new ArrayList<Persona>(personas);
	}
	
	public void buscar(String palabraDeBusqueda){
		personasAux = new ArrayList<Persona>(personas);
		
		for (Persona p: personas) {
			if(!p.getNombre().toLowerCase()
					.contains(palabraDeBusqueda.toLowerCase())&&
					!p.getApellidoPaterno().toLowerCase()
					.contains(palabraDeBusqueda.toLowerCase())&&
					!p.getApellidoMaterno().toLowerCase()
					.contains(palabraDeBusqueda.toLowerCase())) {
				personasAux.remove(p);
			}
		}

	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return personasAux.size();
	}
	
	@Override
	public String getColumnName(int columna){
		if(columna==0) {
			return "Nombre";
		}else if(columna==1) {
			return "Apellido Paterno";
		}else {
			return "Apellido Materno";
		}	
	}

	@Override
	public Object getValueAt(int fila, int columna) {
		// TODO Auto-generated method stub
		if(columna==0) {
			return personasAux.get(fila).getNombre();
		}else if(columna==1) {
			return personasAux.get(fila).getApellidoPaterno();
		}else {
			return personasAux.get(fila).getApellidoMaterno();
		}
	}
}
