package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.database.MysqlConnect;
import model.entities.Persona;

public class PersonaDAO implements IPersonaDAO{
	
	MysqlConnect conn;
	
	public PersonaDAO(){
		conn = new MysqlConnect();
		conn.connect();
	}

	@Override
	public void agregar(Persona p) {
		try {
			PreparedStatement consulta = conn.getConn().
					prepareStatement("INSERT INTO personas"
							+ " (id, nombre, apellidoPaterno, apellidoMaterno)"
							+ " VALUES (null, ?, ?, ?)");
			consulta.setString(1, p.getNombre());
			consulta.setString(2, p.getApellidoPaterno());
			consulta.setString(3, p.getApellidoMaterno());
			
			consulta.executeUpdate();
			consulta.close();
			System.out.println("Persona Guardada Exitosamente");
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void modificar(Persona p) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = conn.getConn().prepareStatement(
					"UPDATE persona SET nombre=?, apellidoPaterno=?" 
			+ ", apellidoMaterno=? WHERE id =?");
			statement.setString(1, p.getNombre());
			statement.setString(2, p.getApellidoPaterno());
			statement.setString(3, p.getApellidoMaterno());
			statement.setInt(4, p.getId());
			statement.executeUpdate();
			statement.close();

		} catch (SQLException e) {

		}

	}

	@Override
	public void eliminar(Persona p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Persona> buscarTodos() {
		List<Persona> list = new ArrayList<Persona>();
		try {
			Statement statement = conn.getConn().createStatement();
			ResultSet result = statement.executeQuery("SELECT * "
					+ "FROM personas");
			while (result.next()) {
				Persona p = new Persona();
				p.setId(result.getInt(1));
				p.setNombre(result.getString("nombre"));
				p.setApellidoPaterno(result.getString("apellidoPaterno"));
				p.setApellidoMaterno(result.getString("apellidoMaterno"));
				list.add(p);
			}
			statement.close();
			result.close();
			return list;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}


	@Override
	public List<Persona> buscarPorNombre(String nombre) {
		List<Persona> list = new ArrayList<Persona>();
		try {
			Statement statement = conn.getConn().createStatement();
			ResultSet result = statement.executeQuery("SELECT * "
					+ "FROM personas WHERE nombre LIKE '%" + nombre + "%'");
			while (result.next()) {
				Persona p = new Persona();
				p.setId(result.getInt(1));
				p.setNombre(result.getString("nombre"));
				p.setApellidoPaterno(result.getString("apellidoPaterno"));
				p.setApellidoMaterno(result.getString("apellidoMaterno"));
				list.add(p);
			}
			statement.close();
			result.close();
			return list;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	public static void main(String args[]) {
		PersonaDAO dao = new PersonaDAO();
		Persona p = new Persona(0, "Julio", "Gomez", "llanes");
		dao.agregar(p);
	}

	@Override
	public Persona buscarPorID(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
