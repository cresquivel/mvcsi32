package model.dao;

import java.util.List;

import model.entities.Persona;

public interface IPersonaDAO {
	
	public void agregar(Persona p);
	
	public void modificar(Persona p);
	
	public void eliminar(Persona p);
	
	public Persona buscarPorID(int id);
	
	public List<Persona> buscarTodos();
	
	public List<Persona> buscarPorNombre(String nombre);

}
